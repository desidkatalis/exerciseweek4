import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import styles from '../Style/styles'
import DatePicker from 'react-native-datepicker'
import { getTaskByDateHandler } from '../Actions/index'
import { connect } from 'react-redux'

class search extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'TASK',
            headerTitleStyle: styles.headerSearch,
            //   headerTitleStyle: styles.headerTitleDetail,
        };
    };
    constructor(props) {
        super(props)
        this.state = {
            date: new Date()
        }
    }
    componentDidMount() {
        console.log('task list', this.props.taskReducer)
        console.log('get task handler', this.props.getTaskByDateHandler())
        console.log('get date', getDate)
        const getDate = this.state.date
        this.props.getTaskByDateHandler()
    }
    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: '100%',
                    backgroundColor: 'rgba(0,0,0,0.5)',
                }}
            />
        );
    };
    renderItem = data => (
        <TouchableOpacity style={styles.listTask}>
            <Text style={styles.listText}>Title: {data.item.title}</Text>
            <Text style={styles.listText}>Description: {data.item.description}</Text>
            <Text style={styles.listText}>Date: {data.item.date}</Text>
            <Text style={styles.listText}>Status: {data.item.status}</Text>
        </TouchableOpacity>
    );
    render() {
        return (
            <View>
                <View style={styles.header}>
                    <View style={styles.option}>
                        <DatePicker
                            style={styles.date}
                            date={this.state.date}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate={new Date()}
                            // maxDate="01-01-2019"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    width: 0,
                                    height: 0
                                },
                                dateInput: {
                                    marginTop: 40,
                                    marginLeft: 36,
                                    borderRadius: 10,
                                    borderWidth: 2,
                                    borderColor: 'black',
                                    width: 200
                                }
                            }}
                            onDateChange={(date) => this.props.getTaskByDateHandler(date)}
                        />
                    </View>
                </View>
                <View>
                    <FlatList
                        data={this.props.taskReducer.taskDate}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.id}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        taskReducer: state.taskReducer
    }
}

export default connect(
    mapStateToProps,
    { getTaskByDateHandler }
)(search)