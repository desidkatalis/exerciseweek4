import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import list from './list';
import search from './search';
import createTask from './createTask';

const MainNavigator = createStackNavigator(
  {
    // Home: {screen: HomeScreen},
    List: {
      screen: list,
    },
    Search: {
      screen: search
    },
    Create: {
      screen: createTask
    }
  },
  {
    initialRouteName: 'List',
  },
);

const Container = createAppContainer(MainNavigator);

export default Container;
