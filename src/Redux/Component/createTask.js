import React, { Component } from 'react';
import { View, Text, TextInput, TouchAbleOpacity, ScrollView, FlatList } from 'react-native'
import styles from '../Style/styles'
import DatePicker from 'react-native-datepicker'
import { connect } from 'react-redux'
import { postHandler } from '../Actions/index'

class createTask extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'CREATE NEW TASK',
            headerTitleStyle: styles.headerSearch,
            //   headerTitleStyle: styles.headerTitleDetail,
        };
    };
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            description: '',
            status: '',
            date: new Date(),
            dateDeadline: new Date(),
            comments: []
        }
    }
    submit = () => {
        console.log('masuk submit')
        const { title, description, status, date, dateDeadline, comments } = this.state
        const titleInput = title
        const descriptionInput = description
        const statusInput = status
        const dateInput = date
        const dateDeadlineInput = dateDeadline
        const commentsInput = comments
        console.log('title', title)
        this.props.postHandler(titleInput, descriptionInput, statusInput, dateInput, dateDeadlineInput, commentsInput)
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <View style={styles.inputPost}>
                        <Text style={styles.text}>Title:</Text>
                        <TextInput
                            style={styles.textInputPost}
                            placeholder="Title"
                            onChangeText={(title) => this.setState({ title: title })}
                            value={this.state.title}
                        />

                        <Text style={styles.text}>Description:</Text>
                        <TextInput
                            style={styles.textInputPost}
                            placeholder="Description"
                            onChangeText={(description) => this.setState({ description: description })}
                            value={this.state.description}
                        />

                        <Text style={styles.text}>Status:</Text>
                        <TextInput
                            style={styles.textInputPost}
                            placeholder="Status"
                            onChangeText={(status) => this.setState({ status: status })}
                            value={this.state.status}
                        />

                        <View style={styles.dateDirection}>
                            <View style={styles.dateFlex}>
                                <Text style={styles.text}>Date</Text>
                                <DatePicker
                                    style={styles.date}
                                    date={this.state.date}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    minDate={new Date()}
                                    // maxDate="01-01-2019"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            width: 0,
                                            height: 0
                                        },
                                        dateInput: {
                                            marginTop: 10,
                                            borderRadius: 10,
                                            borderWidth: 2,
                                            borderColor: 'black',
                                            width: 200
                                        }
                                    }}
                                    onDateChange={(date) => this.setState({ date: date })}
                                />
                            </View>
                            <View style={styles.dateFlex}>
                                <Text style={styles.text}>Date Deadline</Text>
                                <DatePicker
                                    style={styles.date}
                                    date={this.state.dateDeadline}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    minDate={new Date()}
                                    // maxDate="01-01-2019"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            width: 0,
                                            height: 0
                                        },
                                        dateInput: {
                                            marginTop: 10,
                                            borderRadius: 10,
                                            borderWidth: 2,
                                            borderColor: 'black',
                                            width: 200
                                        }
                                    }}
                                    onDateChange={(date) => this.setState({ dateDeadline: date })}
                                />
                            </View>
                        </View>
                        <Text style={styles.text}>Comments:</Text>
                        <TextInput
                            style={styles.textInputPost}
                            placeholder="Comments"
                            onChangeText={(commentsInput) => this.setState({ commentsInput })}
                            onSubmitEditing={() => this.setState({ comments: (this.state.comments).concat(this.state.commentsInput), commentsInput: '' })}
                            value={this.state.comments}
                        />
                        <Text style={styles.text}>Vaccinations:</Text>
                        <FlatList
                            data={this.state.comments}
                            renderItem={data => {
                                return (
                                    <Text style={styles.text}>{data.item}</Text>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />

                        <View>
                            <TouchAbleOpacity onPress={this.submit}>
                                <View style={styles.button}>
                                    <Text style={styles.buttonText}>Button</Text>
                                </View>
                            </TouchAbleOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        postHandler: (title, description, status, date, dateDeadline, comments) => {
            dispatch(postHandler(title, description, status, date, dateDeadline, comments));
        }
    };
};

const mapStateToProps = (state) => {
    return {
        taskReducer: state.taskReducer
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(createTask)