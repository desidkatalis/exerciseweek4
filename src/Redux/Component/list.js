import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator, FlatList, TouchAbleOpacity } from 'react-native'
import { connect } from 'react-redux'
import { getTaskHandler } from '../Actions/index'
import styles from '../Style/styles'

class list extends Component {
    static navigationOptions = {
        header: null
    }
    componentDidMount() {
        console.log('task list')
        console.log(this.props.taskReducer)
        console.log('get task handler')
        console.log(this.props.getTaskHandler())
        this.props.getTaskHandler()
    }
    constructor(props) {
        super(props);
        this.state = {
            taskList: [],
            loading: true,
        };
    }
    renderItem = data => (
        <TouchAbleOpacity style={styles.listTask}>
            <Text style={styles.listText}>Title: {data.item.title}</Text>
            <Text style={styles.listText}>Description: {data.item.description}</Text>
            <Text style={styles.listText}>Date: {data.item.date}</Text>
            <Text style={styles.listText}>Status: {data.item.status}</Text>
        </TouchAbleOpacity>
    );

    render() {
        if (this.props.taskReducer.loading) {
            return (
                <View style={styles.loader}>
                    <ActivityIndicator size="large" color="#0c9" />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.searchHeader}>
                        <TouchAbleOpacity onPress={() => this.props.navigation.navigate('Search')}>
                            <Image style={styles.search} source={require('../../res/search.png')} />
                        </TouchAbleOpacity>
                    </View>
                    <View style={styles.titleHeader}>
                        <Text style={styles.title}>Personal Task Tracker</Text>
                    </View>
                    <View style={styles.addHeader}>
                        <TouchAbleOpacity onPress={() => this.props.navigation.navigate('Create')}>
                            <Image style={styles.add} source={require('../../res/add.png')} />
                        </TouchAbleOpacity>
                    </View>
                </View>
                <View style={styles.body}>
                    <FlatList
                        data={this.props.taskReducer.taskList}
                        // ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.id}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        taskReducer: state.taskReducer
    }
}

export default connect(
    mapStateToProps,
    { getTaskHandler }
)(list)