import { task } from './task';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
  taskReducer: task,
});

const rootReducer = (state, action) => {
  return allReducers(state, action)
}

export default rootReducer;