import {
    GET_ALL_TASK,
    FAILED_GET_ALL_TASK,
    GET_ALL_STARTED,
    GET_TASK_BY_DATE,
    FAILED_GET_BY_DATE,
    POST,
    FAILED_POST
} from '../Actions/types'

const initialState = {
    taskList: [],
    taskDate: [],
    post: [],
    loading: true,
    error: null
}

const task = function (state = initialState, action) {
    switch (action.type) {
        case GET_ALL_STARTED:
            return {
                ...state,
                loading: true
            }
        case GET_ALL_TASK:
            return {
                taskList: action.payload,
                loading: false,
                error: null
            }
        case FAILED_GET_ALL_TASK:
            return {
                taskList: [],
                loading: false,
                error: action.payload.error
            }
        case GET_TASK_BY_DATE:
            return {
                taskDate: action.payload,
                loading: false,
                error: null
            }
        case FAILED_GET_BY_DATE:
            return {
                taskDate: [],
                loading: false,
                error: action.payload.error
            }
        case POST:
            return {
                post: action.payload,
                loading: false,
                error: null
            }
        case FAILED_POST:
            return {
                post: [],
                loading: false,
                error: action.payload.error
            }
        default:
            return state
    }
}

export { task }