import React, { Component } from 'react';
import store from './src/Redux/Store'
import { Provider } from 'react-redux';
import MainNavigator from './Component/MainNavigator';

export default class AppRedux extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainNavigator />
            </Provider>
        );
    }
}
