import rootReducer from '../Reducer/index'
import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'


const middlewares = [thunk]

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middlewares)
    )
)

export default store