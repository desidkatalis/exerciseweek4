import { StyleSheet } from 'react-native'

const one = 1
const thirty = 30
const center = 'center'
const twenty = 20
const two = 2
const white = '#fff'
const styles = StyleSheet.create({
    container: {
        flex: one,
        backgroundColor: white,
    },

    // START STYLE LIST
    header: {
        flexDirection: 'row',
        backgroundColor: 'red',
        height: 100
    },
    body: {
        // backgroundColor: 'yellow'
    },
    searchHeader: {
        marginTop: thirty,
        flex: one
    },
    titleHeader: {
        marginTop: thirty,
        flex: 3
    },
    addHeader: {
        marginTop: 27,
        flex: one
    },
    title: {
        fontSize: twenty,
        textAlign: center
    },
    search: {
        marginLeft: twenty
    },
    add: {
        marginLeft: 25
    },
    listTask: {
        paddingVertical: 10,
        marginTop: 10,
        marginLeft: 55,
        backgroundColor: white,
        borderRadius: 6,
        borderWidth: two,
        borderColor: 'black',
        height: 150,
        width: 300
    },
    listText: {
        margin: two,
        fontSize: 14
    },
    loader: {
        flex: one,
        justifyContent: center,
        alignItems: center,
        backgroundColor: white,
    },
    // END STYLE LIST

    // START STYLE SEARCH
    headerSearch: {
        textAlign: center,
        flex: one,
        fontSize: twenty
    },
    option: {
        flex: one,
        height: 60,
    },
    // END STYLE SEARCH

    // START STYLE CREATE NEW TASK
    inputPost: {

        margin: 50
    },
    textInputPost: {
        borderWidth: 2,
        borderRadius: 3,
        width: 310
    },
    text: {
        margin: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },
    dateDirection: {
        flexDirection: 'row'
    },
    dateFlex: {
        flex: one
    },
    button: {
        width: 300,
        height: 50,
        padding: 15,
        marginTop: thirty,
        marginLeft: 10,
        marginBottom: thirty,
        backgroundColor: "green",
        alignContent: center,
        elevation: 5,
        borderRadius: twenty
    },
    buttonText: {
        color: white,
        alignItems: center,
        textAlign: center,
        fontWeight: "600",
        fontSize: 15
    }
    // END STYLE CREATE NEW TASK
})

export default styles