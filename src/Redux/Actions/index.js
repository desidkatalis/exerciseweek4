import axios from 'axios'
import {
    GET_ALL_TASK,
    FAILED_GET_ALL_TASK,
    GET_ALL_STARTED,
    GET_TASK_BY_DATE,
    FAILED_GET_BY_DATE,
    POST,
    FAILED_POST
} from './types'

const getstarted = (response) => {
    return {
        type: GET_ALL_STARTED
    }
}

// START GET ALL TASK
const getAllTask = (response) => {
    return {
        type: GET_ALL_TASK,
        payload: response
    }
}

const failedGetAllTask = (error) => {
    return {
        type: FAILED_GET_ALL_TASK,
        payload: {
            error
        }
    }
}

const getTaskHandler = () => {
    return (dispatch) => {
        dispatch(getstarted())
        axios
            .get(`http://192.168.0.19:8080/personalTask`, {
                auth: {
                    username: 'desi',
                    password: 'secret'
                }
            })
            .then(response => {
                dispatch(getAllTask(response.data))
            })
            .catch(error =>
                dispatch(failedGetAllTask(error))
            )
    }
}
// END GET ALL TASK

// START GET BY QUERY
const getByDate = (response) => {
    return {
        type: GET_TASK_BY_DATE,
        payload: response
    }
}


const failedGetByDate = (error) => {
    return {
        type: FAILED_GET_BY_DATE,
        payload: {
            error
        }
    }
}

const getTaskByDateHandler = (date) => {
    return (dispatch) => {
        dispatch(getstarted())
        axios
            .get(`http://192.168.0.19:8080/personalTask/date`, {
                auth: {
                    username: 'desi',
                    password: 'secret'
                },
                params: {
                    date: date
                }
            })
            .then(response => {
                dispatch(getByDate(response.data))
            })
            .catch(error =>
                dispatch(failedGetByDate(error))
            )
    }
}
// END GET BY QUERY

// START POST
const postTask = (response) => {
    return {
        type: POST,
        payload: response
    }
}


const failedPost = (error) => {
    return {
        type: FAILED_POST,
        payload: {
            error
        }
    }
}

const postHandler = (title, description, status, date, dateDeadline, comments) => {
    console.log('post handler')
    return (dispatch) => {
        dispatch(getstarted())
        axios
            .post(`http://192.168.0.19:8080/personalTask`,
                {
                    title: title,
                    description: description,
                    status: status,
                    date: date,
                    dueTask: dateDeadline,
                    comments: [
                        {
                            comment: comments
                        }
                    ]
                },
                {
                    auth: {
                        username: 'desi',
                        password: 'secret'
                    }
                }
            )
            .then(response => {
                dispatch(postTask(response.data))
            })
            .catch(error =>
                dispatch(failedPost(error))
            )
    }
}
//END POST

export { getTaskHandler, getTaskByDateHandler, postHandler }